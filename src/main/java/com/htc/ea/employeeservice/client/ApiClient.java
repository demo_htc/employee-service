package com.htc.ea.employeeservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.employeeservice.client.fallback.ApiClientFallback;
import com.htc.ea.employeeservice.model.Department;

/**
 * Interfaz de consumo de APIs mediante el Gateway de Spring
 * Posee implementacion de la interfaz la cual captura los errores de conexion
 * @author htc
 *
 */
@FeignClient(name = "spring-gateway-service" , fallbackFactory = ApiClientFallback.class)
public interface ApiClient {
	
	/**
	 * consulta al microservicio department
	 * retorna el departamento bajo el id asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id departamento
	 * @return response entity con headers y department en el body
	 */
	@GetMapping(value="/departments/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Department> findDepartmentById(@PathVariable("id") Long id);
}
