package com.htc.ea.employeeservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.employeeservice.dto.EmployeeRequest;
import com.htc.ea.employeeservice.model.Employee;
import com.htc.ea.employeeservice.service.EmployeeService;

@RestController
public class EmployeeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private Mapper mapper;
	
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> save(@Valid @RequestBody EmployeeRequest request) {
		LOGGER.info("Employee add: {}", request);
		return employeeService.save(mapper.map(request, Employee.class));
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> findById(@PathVariable("id") Long id) {
		LOGGER.info("Employee found: id={}", id);
		return employeeService.findById(id);
	}
	
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAll() {
		LOGGER.info("Employee find");
		return employeeService.findAll();
	}
	
	@GetMapping(value="/departments/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAllByIdDepartment(@PathVariable("id") Long id) {
		LOGGER.info("Employee find: id={}", id);
		return employeeService.findAllByIdDepartment(id);
	}
	
	@GetMapping(value="/organizations/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAllByIdOrganization(@PathVariable("id") Long id) {
		LOGGER.info("Employee find: organizationId={}", id);
		return employeeService.findAllByIdOrganization(id);
	}
	
	
	@GetMapping(value="/test")
	public ResponseEntity<String> getTest() {
		return new ResponseEntity<>("Hola mundo",HttpStatus.OK);
	}
	
}
